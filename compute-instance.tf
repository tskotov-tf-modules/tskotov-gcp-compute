data "google_compute_image" "my_image" {
  family  = "centos-7"
  project = "centos-cloud"
}

resource "google_compute_instance" "vm" {
  "boot_disk" = {
    initialize_params {
      image = "${data.google_compute_image.my_image.self_link}"
    }
  }

  machine_type = "${var.machine_type}"

  metadata {
    "startup-script" = "${var.startup_script}"
  }

  name = "${var.name}"

  "network_interface" = {
    subnetwork    = "${var.subnet}"
    access_config = {}
  }

  tags = ["${var.tags}"]
  zone = "${var.zone}"
}
