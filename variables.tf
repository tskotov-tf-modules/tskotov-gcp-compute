variable "name" {
  default = "alexandra"
}

variable "machine_type" {
  default = "f1-micro"
}

variable "zone" {}

variable "subnet" {}

variable "tags" {
  default = []
  type    = "list"
}

variable "startup_script" {
  default = ""
}
